# How to build

```
mvn clean install docker:build -Pwith-tests
```

# How to run 

```
docker run --rm -it -p 8081:8080 bank-master-service
```

# How to deploy

Start a current version of Jenkins (https://jenkins.io/doc/pipeline/tour/getting-started/). Make sure that the blueocean plugins are installed: https://jenkins.io/doc/book/blueocean/getting-started/#on-an-existing-jenkins-instance.
Now create a Multibranch Pipeline project and execute. And voila. At the end, bank-master-service shold be running on our machine.

# What resources are exposed?

Echo resource for basic tests:

- http://localhost:8081/echo/Hello

Return the bank information for the bank with clearing number 100:

- http://localhost:8081/banks?bankClearingNumber=100

Return some health status

- http://localhost:8081/health
- http://localhost:8081/node
- http://localhost:8081/threads
- http://localhost:8081/heap

