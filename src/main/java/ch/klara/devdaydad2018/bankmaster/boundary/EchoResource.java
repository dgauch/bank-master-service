package ch.klara.devdaydad2018.bankmaster.boundary;

import java.time.LocalDateTime;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("echo")
public class EchoResource {

    @GET
    @Path("{echostring}")
    @Produces("text/plain")
    public String echo(@PathParam("echostring") String echostring) {
        return LocalDateTime.now() + " received '" + echostring + "'\n";
    }

}
