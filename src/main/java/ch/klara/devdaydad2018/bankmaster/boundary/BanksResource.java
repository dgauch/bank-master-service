package ch.klara.devdaydad2018.bankmaster.boundary;

import ch.klara.devdaydad2018.bankmaster.entity.BankEntity;
import ch.klara.devdaydad2018.bankmaster.entity.BankMasterStore;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("banks")
public class BanksResource {

    @Inject
    BankMasterStore bankMasterStore;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<BankEntity> getBankEntities(@QueryParam("bankClearingNumber") Integer bankClearingNumber) {
        return bankMasterStore.findBanksByBankClearingNumber(bankClearingNumber);
    }

}
