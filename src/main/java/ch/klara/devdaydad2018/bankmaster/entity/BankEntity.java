package ch.klara.devdaydad2018.bankmaster.entity;

import java.util.Objects;

public class BankEntity {

    int groupId;
    int bankClearingNumber;
    int headOffice;
    String branchId;
    String shortName;
    String name;
    String address;
    String zip;
    String place;

    BankEntity() {
    }

    public BankEntity(int groupId, int bankClearingNumber, int headOffice, String branchId, String shortName, String name, String address, String zip, String place) {
        this.groupId = groupId;
        this.bankClearingNumber = bankClearingNumber;
        this.headOffice = headOffice;
        this.branchId = branchId;
        this.shortName = shortName;
        this.name = name;
        this.address = address;
        this.zip = zip;
        this.place = place;
    }

    public int getGroupId() {
        return groupId;
    }

    public int getBankClearingNumber() {
        return bankClearingNumber;
    }

    public int getHeadOffice() {
        return headOffice;
    }

    public String getBranchId() {
        return branchId;
    }

    public String getShortName() {
        return shortName;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getZip() {
        return zip;
    }

    public String getPlace() {
        return place;
    }

    void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    void setBankClearingNumber(int bankClearingNumber) {
        this.bankClearingNumber = bankClearingNumber;
    }

    void setHeadOffice(int headOffice) {
        this.headOffice = headOffice;
    }

    void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    void setShortName(String shortName) {
        this.shortName = shortName;
    }

    void setName(String name) {
        this.name = name;
    }

    void setAddress(String address) {
        this.address = address;
    }

    void setZip(String zip) {
        this.zip = zip;
    }

    void setPlace(String place) {
        this.place = place;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.groupId;
        hash = 53 * hash + this.bankClearingNumber;
        hash = 53 * hash + this.headOffice;
        hash = 53 * hash + Objects.hashCode(this.branchId);
        hash = 53 * hash + Objects.hashCode(this.shortName);
        hash = 53 * hash + Objects.hashCode(this.name);
        hash = 53 * hash + Objects.hashCode(this.address);
        hash = 53 * hash + Objects.hashCode(this.zip);
        hash = 53 * hash + Objects.hashCode(this.place);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BankEntity other = (BankEntity) obj;
        if (this.groupId != other.groupId) {
            return false;
        }
        if (this.bankClearingNumber != other.bankClearingNumber) {
            return false;
        }
        if (this.headOffice != other.headOffice) {
            return false;
        }
        if (!Objects.equals(this.branchId, other.branchId)) {
            return false;
        }
        if (!Objects.equals(this.shortName, other.shortName)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.zip, other.zip)) {
            return false;
        }
        if (!Objects.equals(this.place, other.place)) {
            return false;
        }
        return true;
    }

}
