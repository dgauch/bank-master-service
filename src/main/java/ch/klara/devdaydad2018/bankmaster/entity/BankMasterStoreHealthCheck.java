package ch.klara.devdaydad2018.bankmaster.entity;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;

@Dependent
@Health
public class BankMasterStoreHealthCheck implements HealthCheck {

    @Inject
    BankMasterStore bankMasterStore;

    @Override
    public HealthCheckResponse call() {
        return HealthCheckResponse.builder().name("bank-master-store").state(bankMasterStore.isInitalized()).build();
    }

}
