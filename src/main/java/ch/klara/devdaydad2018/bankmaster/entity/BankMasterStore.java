package ch.klara.devdaydad2018.bankmaster.entity;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.inject.Singleton;

@Singleton
@Startup
public class BankMasterStore {

    final static String BANK_MASTER_FILE_NAME = "bcbankenstamm_e.csv";

    List<BankEntity> banks = new ArrayList<>();

    @PostConstruct
    public void init() {
        final Predicate<String> stringStartsWithDigit = string -> string.charAt(0) >= '0' && string.charAt(0) <= '9';

        InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(BANK_MASTER_FILE_NAME);
        BufferedReader reader = new BufferedReader(new InputStreamReader(resourceAsStream));
        Stream<String> lines = reader.lines();
        banks = lines
                .filter(stringStartsWithDigit)
                .map(this::parseLine)
                .collect(Collectors.toList());
    }

    public int numberOfBanks() {
        return banks.size();
    }

    public List<BankEntity> getAllBanks() {
        return Collections.unmodifiableList(banks);
    }

    public List<BankEntity> findBanksByBankClearingNumber(Integer bankClearingNumber) {
        return banks.stream()
                .filter(bank -> (bankClearingNumber == null) || bank.getBankClearingNumber() == bankClearingNumber)
                .collect(Collectors.toList());
    }

    BankEntity parseLine(String bankMasterLine) {
        String[] items = (bankMasterLine != null) ? bankMasterLine.split(";", -1) : null;
        if (bankMasterLine == null || bankMasterLine.isEmpty() || items.length != 23) {
            throw new IllegalStateException("Invalid line format for bank master: " + bankMasterLine);
        }
        return new BankEntity(Integer.parseInt(items[0]), Integer.parseInt(items[1]), Integer.parseInt(items[5]), items[2], items[11], items[12], items[13], items[15], items[16]);
    }
    
    public boolean isInitalized() {
        return (banks != null && banks.size() > 0);
    }

}
