package ch.klara.devdaydad2018.bankmaster.boundary;

import ch.klara.devdaydad2018.bankmaster.entity.BankEntity;
import java.net.URL;
import java.util.List;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class BanksResourceIT {

    @Test
    @RunAsClient
    public void ping(@ArquillianResource URL baseURL) {
        WebTarget target = ClientBuilder.newBuilder().build().target("http://localhost:8080/banks").queryParam("bankClearingNumber", "100");
        BankEntity referenceBankEntity = new BankEntity(1, 100, 100, "0000", "SNB", "Schweizerische Nationalbank", "Börsenstrasse 15", "8022", "Zürich");

        List<BankEntity> banks = target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<BankEntity>>() {
        });

        assertEquals("Number of banks found: ", 1, banks.size());
        assertEquals("National bank was not found!", referenceBankEntity, banks.get(0));
    }
}
