package ch.klara.devdaydad2018.bankmaster.entity;

import java.util.List;
import java.util.Optional;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class BankMasterStoreTest {

    private BankMasterStore bankMasterStore;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void initBankMasterStore() {
        bankMasterStore = new BankMasterStore();
        bankMasterStore.init();
    }

    @Test
    public void initializingBankMasterData_ShouldInitializeSomeBanks() {
        assertEquals("Wrong number of banks inilialized!", 3346, bankMasterStore.numberOfBanks());
    }

    @Test
    public void correctlyFormattedLine_ShouldParseToBankEntity() {
        BankEntity referenceBankEntity = new BankEntity(1, 100, 100, "0000", "SNB", "Schweizerische Nationalbank", "Börsenstrasse 15", "8022", "Zürich");
        BankEntity bankEntity = bankMasterStore.parseLine("1;100;0000;;001008;100;1;20170911;1;3;1;SNB;Schweizerische Nationalbank;Börsenstrasse 15;Postfach 2800;8022;Zürich;058 631 00 00;;;;30-5-5;SNBZCHZZXXX");

        assertEquals("Bank master line was incorrectly parsed", referenceBankEntity, bankEntity);
    }

    @Test
    public void nullLine_ShouldThrowException() {
        exception.expect(IllegalStateException.class);
        bankMasterStore.parseLine(null);
    }

    @Test
    public void emptyLine_ShouldThrowException() {
        exception.expect(IllegalStateException.class);
        bankMasterStore.parseLine("");
    }

    @Test
    public void invalidLine_ShouldThrowException() {
        exception.expect(IllegalStateException.class);
        bankMasterStore.parseLine(";");
    }

    @Test
    public void listOfAllBanks_ShouldContainSwissNationalBank() {
        BankEntity referenceBankEntity = new BankEntity(1, 100, 100, "0000", "SNB", "Schweizerische Nationalbank", "Börsenstrasse 15", "8022", "Zürich");

        List<BankEntity> banks = bankMasterStore.getAllBanks();

        Optional<BankEntity> nationalBank = banks.stream().filter(bank -> bank.equals(referenceBankEntity)).findFirst();
        assertTrue("National bank was not found!", nationalBank.isPresent());
    }

}
